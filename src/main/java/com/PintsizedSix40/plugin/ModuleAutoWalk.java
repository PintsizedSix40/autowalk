package com.PintsizedSix40.plugin;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.player.UpdatePlayerMoveStateEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.module.settings.ValueBase;

import java.util.HashMap;

public class ModuleAutoWalk extends Module {

  private HashMap<String, Boolean> requiredPlugins = new HashMap<>();

  public ModuleAutoWalk() {
    super("AutoWalk", ModuleCategory.MOVEMENT);

    this.setVersion("1.0");
    this.setBuildVersion(18300);
    this.setDescription("Makes you walk forward.");
    this.setRegistered(true);

    for (String plugin : new String[]{"ScaffoldWalk", "Step"}) {
      this.requiredPlugins.put(plugin, false);
    }

    this.getModuleValues().put("speed", new ValueBase("Speed", 0.75F, 0.1F, 1.0F));
  }

  @Override
  public void onEnable() {
    for (String plugin : this.requiredPlugins.keySet()) {
      if (!Wrapper.getModuleManager().doesModuleExist(plugin)) {
        Chat.printClientMessage(String.format("Install %s to enhance AutoWalk", plugin));
      } else if (!Wrapper.getModuleManager().isModuleActive(plugin)) {
        Wrapper.getModuleManager().getModule(plugin).setEnabled(true);
        this.requiredPlugins.put(plugin, true);
      }
    }
  }

  @EventHandler
  public void onUpdateMoveState(UpdatePlayerMoveStateEvent updateEvent) {
    updateEvent.setMoveForward(this.getValueBase("speed").getValue());
    if (!Wrapper.getGameSettings().keyBindSneak.isKeyDown() && Wrapper.getPlayer().isInWater()) {
      Wrapper.getPlayer().motionY = 0.04D;
    }

    if (Wrapper.getPlayer().motionX == 0.0D && Wrapper.getPlayer().motionZ == 0.0D) {
      Wrapper.getPlayer().rotationYaw += 90.0F;
    }
  }

  @Override
  public void onDisable() {
    this.requiredPlugins.forEach((plugin, state) -> {
      if (state) {
        Wrapper.getModuleManager().getModule(plugin).setEnabled(false);
        this.requiredPlugins.put(plugin, false);
      }
    });
  }
}
  
 

